package com.task62.provincedistrictward.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.task62.provincedistrictward.model.*;
import com.task62.provincedistrictward.repository.*;


@RequestMapping("/")
@RestController
@CrossOrigin(value = "*" , maxAge = -1)
public class AllController {
    @Autowired
    IProvinceRepository provinceRepository;
    @Autowired
    IDistrictRepository districtRepository;
    
    @GetMapping("/devcamp-provinces")
    public ResponseEntity<List<CProvince>> getProvinceList() {
        try {
            List<CProvince> provinceList = new ArrayList<CProvince>();
            provinceRepository.findAll().forEach(provinceList::add);
            return new ResponseEntity<List<CProvince>>(provinceList, HttpStatus.OK);
        } catch(Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/devcamp-districts")
    public ResponseEntity<Set<CDistrict>> getDistrictListByProvinceCode(@RequestParam(required = true) String provinceCode) {
        try {
            CProvince vProvince = provinceRepository.findByCode(provinceCode);
            if(vProvince != null) {
                return new ResponseEntity<>(vProvince.getDistricts(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch(Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/devcamp-wards")
    public ResponseEntity<Set<CWard>> getWardListByDistrictId(@RequestParam(required = true) int districtId) {
        try {
            CDistrict vDistrict = districtRepository.findById(districtId);
            if(vDistrict != null) {
                return new ResponseEntity<>(vDistrict.getWards(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch(Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
