package com.task62.provincedistrictward.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task62.provincedistrictward.model.CDistrict;

public interface IDistrictRepository extends JpaRepository<CDistrict, Integer>{
    CDistrict findById(int id);
}
