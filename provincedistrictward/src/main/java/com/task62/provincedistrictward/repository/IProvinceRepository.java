package com.task62.provincedistrictward.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task62.provincedistrictward.model.CProvince;

public interface IProvinceRepository extends JpaRepository<CProvince, Integer>{
    CProvince findByCode(String code);
}
