package com.task62.provincedistrictward;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProvincedistrictwardApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProvincedistrictwardApplication.class, args);
	}

}
